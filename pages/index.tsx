import produce from "immer";
import { useCallback, useState } from "react";

type Cell = {
    value: string,
    revealed: boolean,
    timeout?: NodeJS.Timeout,
};

function shuffle<T>(array: T[]): T[] {
    let currentIndex = array.length,  randomIndex;
  
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
  
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
  
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
  
    return array;
}

function generateCells(): Cell[] {
    let chars = ['😀', '😍', '🥸', '😈', '😒', '😝'];
    let doubled: string[] = [];

    chars.forEach(char => {
        doubled.push(char, char);
    });
    shuffle(doubled);
    return doubled.map(value => {
        return { value, revealed: false };
    });
}

function useCells(generator: () => Cell[]) {
    let [ cells, setCells ] = useState<Cell[]>(generator);
    let [ won, setWon ] = useState(false);

    let revealedIndexWithValue = useCallback((value: string): number | undefined => {
        let idx = cells.findIndex(cell => cell.revealed && cell.value === value);
        if(idx < 0) return undefined;
        return idx;
    }, [cells]);

    let hideCell = useCallback((index: number): void => {
        setCells(produce(draft => {
            draft[index].revealed = false;
            let oldTimeout = draft[index].timeout;
            if(oldTimeout) clearTimeout(oldTimeout);
            draft[index].timeout = undefined;
        }));
    }, [setCells]);
    
    let revealCell = useCallback((index: number): void => {
        let cellToReveal = cells[index];
        if(cellToReveal.revealed) return;

        if(cells.filter(cell => cell.revealed).length == cells.length - 1) {
            // everyone except this cell (which is to be revealed) is already revealed
            setWon(true);
        }

        let existingRevealed = revealedIndexWithValue(cellToReveal.value);
        if(existingRevealed) {
            setCells(cells => {
                let oldTimeout = cells[existingRevealed!].timeout;
                if(oldTimeout) clearTimeout(oldTimeout);

                return cells;
            });

            setCells(produce(draft => {
                draft[index].revealed = true;
                draft[existingRevealed!].timeout = undefined;
            }));

            return;
        }

        let hider = setTimeout(() => hideCell(index), 2000);

        setCells(produce(draft => {
            draft[index].revealed = true;
            draft[index].timeout = hider;
        }));
    }, [cells, setCells, hideCell, revealedIndexWithValue, setWon]);

    let restartGame = useCallback(() => {
        setCells(generateCells());
        setWon(false);
    }, [setWon, setCells]);

    return { cells, revealCell, won, restartGame }
}

export default function Home() {
    let { cells, revealCell, won, restartGame } = useCells(generateCells);

    let items = cells.map((cell, index) => {
        let text = "??"
        let classes = ["cell"];

        if(cell.revealed) {
            classes.push("revealed");
            text = cell.value;
        } else {
            classes.push("hidden");
        }

        return <div className={classes.join(" ")} key={index.toString()} onClick={() => revealCell(index)}>{text}</div>;
    });

    let footer = <></>;
    if(won) {
        footer = <div className="footer">
            <h1>You Won</h1>
            <button onClick={restartGame}>Restart</button>
        </div>;
    }

    return <>
        <div className="container">
            {items}
        </div>
        {footer}
    </>
}
